#! /usr/bin/lua5.1
--includeing cjson to make data useable by other programs
json=require 'cjson'
--declares ac
local ac = 1
--declares ap list
local aplst={}
--patterns for matching tokens
local mch = {}
mch.bss = 'BSS (%x%x[:]%x%x[:]%x%x[:]%x%x[:]%x%x[:]%x%x)[(]on %w+[)]'
--ssid must be seprate to deal whith hidden ssid
mch.ssid='%s*SSID[:](.*)$'
--this should match most other lines
mch.kv='%s*(.+)[:](.+)'
--call back functions for parser
local cb = {}
--starts ap table and adds mac entry on scanning BSS line
function cb.mkap(cp1)
    local ap={}
    ap.mac=cp1
    local ac=#aplst+1
    aplst[ac]=ap
    return ac
end
--ssid function deals whith hidden ssid
function cb.ssid(cp1,ac)
    local ssid
    if cp1==' ' or cp1=='' then
        ssid='hidden'
    else
        ssid=cp1
    end
    aplst[ac].ssid=ssid
end
--for most lines that arnt going to resault in tables themselvs
function cb.kv(cp1,cp2,ac)
    local k,v = cp1,cp2
    aplst[ac][k]=v
end
--actual parssing loop gonna hard code for now
local function parse()
    local f=io.open('/dev/stdin','r')
    for l in f:lines() do
        local c = string.match(l,mch.bss)
        if c then
            ac=cb.mkap(c)
        else
            local s = string.match(l,mch.ssid)
            if s then
                cb.ssid(s,ac)
            else
                local k,v = string.match(l,mch.kv)
                if k and v then
                    cb.kv(k,v,ac)
                end
            end
        end
    end
    f:close()
end
parse()
local lst=json.encode(aplst)
local f=io.open('output.json','w')
f:write(lst)
f:close()
return aplst